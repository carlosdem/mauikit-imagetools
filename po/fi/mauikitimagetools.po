# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the mauikit-imagetools package.
# Tommi Nieminen <translator@legisign.org>, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: mauikit-imagetools\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:29+0000\n"
"PO-Revision-Date: 2023-03-06 13:45+0200\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: src/controls.5/image2text/OCRPage.qml:35
#: src/controls.6/image2text/OCRPage.qml:35
#, kde-format
msgid "Read Area"
msgstr "Lue alue"

#: src/controls.5/image2text/OCRPage.qml:47
#: src/controls.6/image2text/OCRPage.qml:47
#, kde-format
msgid "Read All"
msgstr "Lue kaikki"

#: src/controls.5/image2text/OCRPage.qml:59
#: src/controls.6/image2text/OCRPage.qml:59
#, kde-format
msgid "Configure"
msgstr "Asetukset"

#: src/controls.5/ImageEditor.qml:41 src/controls.6/ImageEditor.qml:41
#, kde-format
msgid "Color"
msgstr "Väri"

#: src/controls.5/ImageEditor.qml:48 src/controls.6/ImageEditor.qml:48
#, kde-format
msgid "Transform"
msgstr "Muunnos"

#: src/controls.5/ImageEditor.qml:55 src/controls.6/ImageEditor.qml:55
#, kde-format
msgid "Layer"
msgstr "Kerros"

#: src/controls.5/ImageInfoDialog.qml:102
#: src/controls.6/ImageInfoDialog.qml:102
#, kde-format
msgid "Edit"
msgstr "Muokkaa"

#: src/controls.5/ImageInfoDialog.qml:103
#: src/controls.6/ImageInfoDialog.qml:103
#, kde-format
msgid "Editing Exif tag"
msgstr "Muokataan Exif-tunnistetta"

#: src/controls.5/ImageInfoDialog.qml:109
#: src/controls.6/ImageInfoDialog.qml:109
#, kde-format
msgid "Tag key"
msgstr "Tunnisteen avain"

#: src/controls.5/ImageInfoDialog.qml:116
#: src/controls.6/ImageInfoDialog.qml:116
#, kde-format
msgid "Tag value"
msgstr "Tunnisteen arvo"

#: src/controls.5/ImageInfoDialog.qml:127
#: src/controls.6/ImageInfoDialog.qml:127
#, kde-format
msgid "Could not edit the tag"
msgstr "Tunnistetta ei voi muokata"

#: src/controls.5/ImageInfoDialog.qml:150
#: src/controls.6/ImageInfoDialog.qml:150
#, kde-format
msgid "Remove"
msgstr "Poista"

#: src/controls.5/ImageInfoDialog.qml:151
#: src/controls.6/ImageInfoDialog.qml:151
#, kde-format
msgid "Are you sure you want to remove the Exif tag %1?"
msgstr "Haluatko varmasti poistaa Exif-tunnisteen %1?"

#: src/controls.5/ImageInfoDialog.qml:160
#: src/controls.6/ImageInfoDialog.qml:160
#, kde-format
msgid "Could not remove the tag"
msgstr "Tunnistetta ei voi poistaa"

#: src/controls.5/ImageInfoDialog.qml:181
#: src/controls.6/ImageInfoDialog.qml:181
#, kde-format
msgid "Details"
msgstr "Yksityiskohdat"

#: src/controls.5/ImageInfoDialog.qml:182
#: src/controls.6/ImageInfoDialog.qml:182
#, kde-format
msgid "File information"
msgstr "Tiedoston tiedot"

#: src/controls.5/private/ColourBar.qml:58
#: src/controls.6/private/ColourBar.qml:58
#, kde-format
msgctxt "@action:button Crop an image"
msgid "Saturation"
msgstr "Kylläisyys"

#: src/controls.5/private/ColourBar.qml:67
#: src/controls.6/private/ColourBar.qml:67
#, kde-format
msgctxt "@action:button Rotate an image"
msgid "Contrast"
msgstr "Kontrasti"

#: src/controls.5/private/ColourBar.qml:76
#: src/controls.6/private/ColourBar.qml:76
#, kde-format
msgctxt "@action:button Rotate an image"
msgid "Exposure"
msgstr "Valotus"

#: src/controls.5/private/ColourBar.qml:85
#: src/controls.6/private/ColourBar.qml:85
#, kde-format
msgctxt "@action:button Rotate an image"
msgid "Highlights"
msgstr "Korostukset"

#: src/controls.5/private/ColourBar.qml:94
#: src/controls.6/private/ColourBar.qml:94
#, kde-format
msgctxt "@action:button Rotate an image"
msgid "Shadows"
msgstr "Varjot"

#: src/controls.5/private/ColourBar.qml:104
#: src/controls.6/private/ColourBar.qml:104
#, kde-format
msgctxt "@action:button Rotate an image"
msgid "Brightness"
msgstr "Kirkkaus"

#: src/controls.5/private/TransformationBar.qml:33
#: src/controls.6/private/TransformationBar.qml:33
#, kde-format
msgctxt "@action:button Mirror an image vertically"
msgid "Flip"
msgstr "Kierrä"

#: src/controls.5/private/TransformationBar.qml:41
#: src/controls.6/private/TransformationBar.qml:41
#, kde-format
msgctxt "@action:button Mirror an image horizontally"
msgid "Mirror"
msgstr "Peilaa"

#: src/controls.5/private/TransformationBar.qml:53
#: src/controls.6/private/TransformationBar.qml:53
#, kde-format
msgctxt "@action:button Rotate an image 90°"
msgid "Rotate 90°"
msgstr "Kierrä 90°"

#: src/controls.5/private/TransformationBar.qml:110
#: src/controls.6/private/TransformationBar.qml:110
#, kde-format
msgctxt "@action:button Crop an image"
msgid "Crop"
msgstr "Rajaa"

#: src/controls.5/private/TransformationBar.qml:118
#: src/controls.6/private/TransformationBar.qml:118
#, kde-format
msgctxt "@action:button Rotate an image"
msgid "Rotate"
msgstr "Kierrä"
